#include <fcntl.h>
#include <unistd.h>

typedef int handle_t;
handle_t stdin_handle = STDIN_FILENO;
#define BUFFER_SIZE 1024
char buffer[BUFFER_SIZE];
int main(void)
{
  handle_t file_handle = open("file.txt", O_WRONLY);
  size_t bytes_read = read(stdin_handle, buffer, BUFFER_SIZE);
  write(file_handle, buffer, bytes_read);
}
