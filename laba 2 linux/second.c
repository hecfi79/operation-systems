#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

typedef int handle_t;
#define BUFFER_SIZE 16
char buffer[BUFFER_SIZE];
int main(void) 
{
  handle_t handle_1 = open("file.txt", O_RDONLY), handle_2 = dup(handle_1), handle_3 = open("file.txt", O_RDONLY)
  printf("handle #1 is %d\n", handle_1);
  printf("handle #2 is %d\n", handle_2);
  printf("handle #3 is %d\n", handle_3);
  lseek(handle_1, 10, SEEK_SET);
  size_t bytes_read = 10;
  while (bytes_read != 0)
  {
    bytes_read = read(handle_1, buffer, BUFFER_SIZE);
    if (bytes_read != 0)
    {
      printf("handle_1: %.*s\n", (int) bytes_read, buffer);
    }
    bytes_read = read(handle_2, buffer, BUFFER_SIZE);
    if (bytes_read != 0)
    {
      printf("handle_2: %.*s\n", (int) bytes_read, buffer);
    }
    bytes_read = read(handle_3, buffer, BUFFER_SIZE);
    if (bytes_read != 0)
    {
      printf("handle_3: %.*s\n", (int) bytes_read, buffer);
    }
  }
  return 0;
}
