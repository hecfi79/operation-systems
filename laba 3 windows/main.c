#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
  HANDLE out_handle = GetStdHandle(STD_OUTPUT_HANDLE), file_handle = CreateFile("file.txt", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  while (file_handle == INVALID_HANDLE_VALUE) 
  {
    DWORD err = GetLastError();
    SetConsoleTextAttribute(out_handle, FOREGROUND_RED);
    printf("Cannot open file: code of error: %lu\n", err);
    if (err == ERROR_SHARING_VIOLATION)
    {
      SetConsoleTextAttribute(out_handle, FOREGROUND_RED);
      printf("ERROR_SHARING_VIOLATION, wait for 3 sec\n");
    } 
    else if (err == ERROR_FILE_NOT_FOUND)
    {
      SetConsoleTextAttribute(out_handle, FOREGROUND_RED);
      printf("ERROR_FILE_NOT_FOUND, aborting\n");
      return -1;
    }
    Sleep(2000);
    file_handle = CreateFile("file.txt", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  }
  SetConsoleTextAttribute(out_handle, FOREGROUND_RED);
  printf("File open\n");
  SetConsoleTextAttribute(out_handle, FOREGROUND_GREEN);
  DWORD bytes_read = -1;
  printf("reading\n");
  while (bytes_read != 0) 
  {
    char buffer[1024];
    ReadFile(file_handle, buffer, 1024, &bytes_read, NULL);
    printf("%.*s\n", (int) bytes_read, buffer);
  }
  return 0;
}
