#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

typedef int handle_t;
int main(void)
{
  handle_t file_handle = open("file.txt", O_RDWR);
  if (file_handle == -1)
  {
    printf("\033[0;31mCannot open file, errno:%d", errno);
    return -1;
  }
  struct flock lock;
  lock.l_type = F_WRLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start = 0;
  lock.l_len = 0;
  int result = fcntl(file_handle, F_SETLK, &lock);
  while (result == -1)
  {
    printf("\033[0;31mcant lock file, i will try again in 2 seconds\n");
    sleep(2);
    result = fcntl(file_handle, F_SETLK, &lock);
  }
  printf("\033[0;31mfile lock successful! starting to read...\n");
  char buffer[1024];
  int bytes_read = -1;
  while (bytes_read != 0)
  {
    bytes_read = read(file_handle, buffer, 1024);
    printf("\033[0;32m%.*s", bytes_read, buffer);
  }
  printf("\n\033[0;31mstart to sleep for 8 seconds\n");
  sleep(8);
  return 0;
}
