#include "Windows.h"
#include <stdio.h>

HANDLE in_handle, out_handle;
DWORD saved_old_mode;
void error_exit(char*);
void key_event_proc(KEY_EVENT_RECORD);
void mouse_event_proc(MOUSE_EVENT_RECORD);
void resize_event_proc(WINDOW_BUFFER_SIZE_RECORD);
int main(void)
{
  in_handle = GetStdHandle(STD_INPUT_HANDLE);
  if (in_handle == INVALID_HANDLE_VALUE)
  {
    error_exit("GetStdHandle");
  }
  out_handle = GetStdHandle(STD_OUTPUT_HANDLE);
  if (in_handle == INVALID_HANDLE_VALUE) 
  {
    error_exit("GetStdHandle");
  }
  if (!GetConsoleMode(in_handle, &saved_old_mode)) 
  {
    error_exit("GetConsoleMode");
  }
  DWORD mode = ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT | ENABLE_EXTENDED_FLAGS;
  if (!SetConsoleMode(in_handle, mode))
  {
    error_exit("SetConsoleMode");
  }
  HANDLE file_handle = NULL;
  file_handle = CreateFile("file.txt", GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL); 
  if (file_handle == INVALID_HANDLE_VALUE)
  {
    error_exit("CreateFile");
  }
  char buffer[1024];
  DWORD bytes_read = 0;
  if (!ReadFile(file_handle, buffer, 1024, &bytes_read, NULL))
  {
    error_exit("ReadFile");
  }
  printf("%.*s", (int)bytes_read, buffer);
  CloseHandle(file_handle);
  INPUT_RECORD input_buffer;
  DWORD useless_variable = 0;
  while (1)
  {
    if (!ReadConsoleInput(in_handle, &input_buffer, 1, &useless_variable))
    {
      error_exit("ReadConsoleInput");
    }
    switch (input_buffer.EventType)
    {
      case MOUSE_EVENT:
        mouse_event_proc(input_buffer.Event.MouseEvent);
        break;
      case WINDOW_BUFFER_SIZE_EVENT:
      case KEY_EVENT:
      case FOCUS_EVENT:
      case MENU_EVENT:
      default:
        break;
    }
  }
  SetConsoleMode(in_handle, saved_old_mode);
  return 0;
}

void error_exit(char* message)
{
  fprintf(stderr, "[ERROR] %s\n", message);
  SetConsoleMode(in_handle, saved_old_mode);
  ExitProcess(1);
}

void mouse_event_proc(MOUSE_EVENT_RECORD mer)
{
  if (mer.dwEventFlags == 0)
  {
    if (mer.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED)
    {
      SMALL_RECT source_pos = { .Top    = mer.dwMousePosition.Y, .Left   = mer.dwMousePosition.X, .Bottom = mer.dwMousePosition.Y, .Right  = mer.dwMousePosition.X };
      COORD buffer_size = { .X = 1, .Y = 1, };
      COORD buffer_pos = { .X = 0, .Y = 0, };
      CHAR_INFO buffer[1];
      BOOL success = ReadConsoleOutput(out_handle, buffer, buffer_size, buffer_pos, &source_pos);
      if (!success)
      {
        error_exit("ReadConsoleOutput");
      }
      char symbol = buffer->Char.AsciiChar;
      if (isalpha(symbol))
      {
        if (symbol == toupper(symbol))
        {
          buffer->Attributes = FOREGROUND_GREEN;
          buffer->Char.AsciiChar = (char) tolower(symbol);
        }
        else
        {
          buffer->Attributes = FOREGROUND_RED;
          buffer->Char.AsciiChar = (char) toupper(symbol);
        }
      }
      success = WriteConsoleOutput(out_handle, buffer, buffer_size, buffer_pos, &source_pos);
      if (!success)
      {
        error_exit("WriteConsoleOutput");
      }
    }
    else if (mer.dwButtonState == RIGHTMOST_BUTTON_PRESSED)
    {
      ExitProcess(0);
    }
  }
}
