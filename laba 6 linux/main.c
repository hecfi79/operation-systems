#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#define _GNU_SOURCE
#include <sched.h>
#define child_stack_size 2048 * 8 * 1024
char child_stack1[child_stack_size];
char child_stack2[child_stack_size];
char child_stack3[child_stack_size];
char child_stack4[child_stack_size];
char child_stack5[child_stack_size];

int cain_function(char *name)
{
  for (int i = 0; i < 20; i += 1)
  {
    printf("\033[0;33mpid: %d, name: %s\n", getpid(), name);
    usleep(1.2 * 1000000);
  }
  return 0;
}

pid_t grand_child;
void term_handler(int signum)
{
  kill(grand_child, SIGKILL);
  exit(signum);
}

int adam_function(char *name)
{
  if (signal(SIGTERM, term_handler) == SIG_IGN)
  {
    signal(SIGTERM, SIG_IGN);
  }
  grand_child = clone(cain_function, child_stack1 + child_stack_size, SIGCHLD, "Cain");
  for (int i = 0; i < 20; i += 1)
  {
    printf("\033[0;34mpid: %d, name: %s\n", getpid(), name);
    usleep(0.9 * 1000000);
  }
  waitpid(grand_child, NULL, 0);
  return 0;
}

int abel_function(char *name) {
  for (int i = 0; i < 15; i += 1)
  {
    printf("\033[0;31mpid: %d, name: %s\n", getpid(), name);
    usleep(1.3 * 1000000);
  }
  return 0;
}

int eve_function(char *name)
{

  pid_t grand_child = clone(abel_function, child_stack1 + child_stack_size, SIGCHLD, "Abel");
  for (int i = 0; i < 20; i += 1)
  {
    printf("\033[0;35mpid: %d, name: %s\n", getpid(), name);
    usleep(0.8 * 1000000);
  }
  waitpid(grand_child, NULL, 0);
  return 0;
}

int main(void)
{
  pid_t first_child = clone(adam_function, child_stack1 + child_stack_size, SIGCHLD, "AdamREEE");

  pid_t second_child = clone(eve_function, child_stack2 + child_stack_size, SIGCHLD, "Eve");

  for (int i = 0; i < 15; i += 1)
  {
    printf("\033[0;37mparent_pid: %d,", getpid());
    printf("\033[0;37m first_child: %d,", first_child);
    printf("\033[0;37m second_child: %d\n", second_child);
    usleep(1000000);
    if (i == 7)
    {
      printf("\033[0;37mkilling Adam\n");
      kill(first_child, SIGTERM);
    }
    if (i == 11)
    {
      printf("\033[0;37mkilling Eve\n");
      kill(second_child, SIGTERM);
    }
  }
  waitpid(first_child, NULL, 0);
  waitpid(second_child, NULL, 0);
  sleep(4);
  return 0;
}
